﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Backend_chat.Controllers
{
    // [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository Repo;
        private readonly IConfiguration Config;

        public AuthController(IAuthRepository repo, IConfiguration config)
        {
            this.Repo = repo;
            this.Config = config;
        }
        [HttpPost("register")]
        public async Task<IActionResult> Register(UserForRegister ufr)
        {

            ufr.Username = ufr.Username.ToLower();
            if (await Repo.UserExists(ufr.Username))
                return BadRequest("Username already exists");
            Users NewUser = new Users
            {
                Username = ufr.Username,
                Name = ufr.Name,
                Surname = ufr.Surname,
                AccountType = "student"
            };
            Users CreatedUSer = await Repo.Register(NewUser, ufr.Password);
            return StatusCode(201);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLogin ufl)
        {
            Users ExistingUser = await Repo.Login(ufl.Username.ToLower(), ufl.Password);

            if (ExistingUser == null)
                return Unauthorized();

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, ExistingUser.Id.ToString()),
                new Claim(ClaimTypes.Name, ExistingUser.Username)
               , new Claim(ClaimTypes.Role, ExistingUser.AccountType)
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.Config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var TokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds,

            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(TokenDescriptor);

            return Ok(new
            {
                token = tokenHandler.WriteToken(token)
            });


        }
    }
}

